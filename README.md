# dgraph

A wrapper to run dgraph queries/mutations.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  dgraph:
    github: your-github-user/dgraph
```

## Usage

```crystal
require "dgraph"

c = Dgraph::Connection.new("0.0.0.0", 8080)
qry = "{me(func: has(starring)) { name }}"
puts c.query(qry) # => json str
```

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://github.com/your-github-user/dgraph/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [your-github-user](https://github.com/your-github-user) Martins Talbergs - creator, maintainer

