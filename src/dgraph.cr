require "./lib/*"

# A wrapper to execute queries
module Dgraph
  VERSION = "0.1.0"

  class Connection < Connection
  end
end
