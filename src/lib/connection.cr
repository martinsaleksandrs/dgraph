require "socket"

# A connection instance may toss queries
# onto endpoint and return bare responses
# json in json out
class Connection
  def initialize(@host : String, @port : UInt32)
  end

  {% for path in %w(alter query mutate) %}
      def {{path.id}}(qry : String)
          post "{{path.id}}", qry
      end
    {% end %}

  def post(path : String, body : String) : String
    client = TCPSocket.new(@host, @port)

    client << [
      "POST /#{path} HTTP/1.1",
      "Host: #{@host}:#{@port}",
      "Content-Length: #{body.size}",
      "",
      body,
    ].join("\r\n")

    if client.gets != "HTTP/1.1 200 OK"
      # exception
    end
    client.gets("\r\n\r\n")

    res = client.gets_to_end
    client.close

    res
  end
end
